export interface Config {
    stationInfoUrl: string;
    stationDataUrl: string;
}

