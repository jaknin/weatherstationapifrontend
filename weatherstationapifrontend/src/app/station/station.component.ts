import { Component, OnInit } from '@angular/core';
import { Config } from './config';
import { StationService } from './station.service';
import { combineLatest } from 'rxjs';
import { StationInfo } from './station-info';

@Component({
  selector: 'app-station',
  templateUrl: './station.component.html',
  styleUrls: ['./station.component.css']
})
export class StationComponent implements OnInit {
  config: Config;
  stationInfo: StationInfo[];

  constructor(private service: StationService) {}

  ngOnInit() {
    combineLatest(this.service.getConfig(), this.service.getStationInfo()).subscribe(([data, stationInfo]) => {
      this.config = data;
      this.stationInfo = stationInfo
    });
  }
}
