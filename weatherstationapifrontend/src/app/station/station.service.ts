import { Injectable } from '@angular/core';
import { HttpClient } from '@angular//common/http';
import { Observable } from 'rxjs';
import { Config } from './config';
import { StationInfo } from './station-info';

@Injectable({
  providedIn: 'root'
})
export class StationService {

  constructor(private http: HttpClient) {}
  url = '../assets/config.json';
  public getConfig(): Observable<Config> {
    return this.http.get<Config>(this.url);
  }

  public getStationInfo(): Observable<StationInfo[]> {
      return this.http.get<StationInfo[]>('http://localhost:8082/info');
  }
}
