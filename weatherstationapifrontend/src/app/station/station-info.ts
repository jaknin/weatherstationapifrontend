import { Station } from './station-data';

export class StationInfo {
    id: number;
    location: string;
    stationData: Station;
}
